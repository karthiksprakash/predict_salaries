import numpy as np
import pandas as pd
from sklearn.metrics import explained_variance_score, r2_score
from sklearn.metrics import make_scorer
from sklearn.grid_search import GridSearchCV
from sklearn.cross_validation import ShuffleSplit
from sklearn.preprocessing import RobustScaler
from time import time
from pprint import pprint as pp


def train_predict(learner, sample_size, X_train, y_train, X_test, y_test): 
    '''
    inputs:
       - learner: the learning algorithm to be trained and predicted on
       - sample_size: the size of samples (number) to be drawn from training set
       - X_train: features training set
       - y_train: income training     
       - X_test: features testing set
       - y_test: income testing set
    '''
    
    results = {}
    
    # Fit the learner to the training data using slicing with 'sample_size'
    start = time() # Get start time
    learner = learner.fit(X_train.iloc[:sample_size], y_train[:sample_size])
    end = time() # Get end time
    
    # Calculate the training time
    results['train_time'] = end - start

    sample_size_for_score = 1000
        
    # Get the predictions on the test set,
    #       then get predictions on the first 300 training samples
    start = time() # Get start time
    predictions_test = learner.predict(X_test)
    predictions_train = learner.predict(X_train[:sample_size_for_score])
    end = time() # Get end time
    
    # Calculate the total prediction time
    results['pred_time'] = end - start
            
    # Compute accuracy on the first 1000 training samples
    results['explained_variance_train'] = explained_variance_score(y_train[:sample_size_for_score], predictions_train)
        
    # Compute accuracy on test set
    results['explained_variance_test'] = explained_variance_score(y_test, predictions_test)
    
    # Compute F-score on the the first 1000 training samples
    results['r2_score_train'] = r2_score(y_train[:sample_size_for_score], predictions_train)
        
    # Compute F-score on the test set
    results['r2_score_test'] = r2_score(y_test, predictions_test)
       
    # Success
    print("{} trained on {} samples.".format(learner.__class__.__name__, sample_size))
        
    # Return the results
    return results



def get_tuned_estimator(X_train, y_train, X_test, y_test, estimator, grid_search_cv_params, scorer, random_state=0, n_jobs=4):

    # Perform grid search on the classifier using 'scorer' as the scoring method
    cv_sets = ShuffleSplit(X_train.shape[0], n_iter = 10, test_size = 0.20, random_state = random_state)
    grid_obj = GridSearchCV(estimator, grid_search_cv_params, scorer, cv=cv_sets, n_jobs=n_jobs)

    # Fit the grid search object to the training data and find the optimal parameters
    start = time() # Get start time
    grid_fit = grid_obj.fit(X_train, y_train)
    end = time() # Get end time
    print("GridSearch time: {}".format(end - start))
    print("Best params: \n")
    pp(grid_obj.best_params_)

    # Get the estimator
    return grid_fit.best_estimator_


def preprocess_features(data, drop_cols, numerical_cols, numerical_scalar):
    #Extract numeric job ID and set it as index.
    data['jobId'] = data['jobId'].str.extract(r'^JOB(\d+)', expand=False)
    data['jobId'] = pd.to_numeric(data['jobId'])
    data.set_index('jobId', inplace=True)
    
    #Drop columns that are not input to the model
    data.drop(columns=drop_cols, axis=1, inplace=True)

    # Initialize a scaler, then apply it to the features        
    data[numerical_cols] = numerical_scalar.fit_transform(data[numerical_cols])

    #One-hot encode categorical values.    
    data = pd.get_dummies(data)
    return data